﻿using Microsoft.Extensions.Caching.Memory;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    /// <summary>
    /// Default values used by API key authentication.
    /// </summary>
    public static class ApiKeyDefaults
    {
        /// <summary>
        /// The default value used for <see cref="ApiKeyOptions.AuthenticationScheme"/>.
        /// </summary>
        public const string AuthenticationScheme = "ApiKey";

        /// <summary>
        /// The default value used for <see cref="ApiKeyOptions."/>.
        /// </summary>
        public static readonly Type StoreSessionType = typeof(MemoryCache);

        /// <summary>
        /// Time to live in cache for sessions
        /// </summary>
        public static readonly TimeSpan ExpireTimeSpan = TimeSpan.FromMinutes(30);


    }
}
