﻿using System.Runtime.Serialization;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Exceptions
{
    /// <summary>
    /// Represents a API key exception.
    /// </summary>
    [Serializable]
    public class ApiKeyException : Exception
    {
        /// <inheritdoc/>
        public ApiKeyException()
        {
        }

        /// <inheritdoc/>
        public ApiKeyException(string? message) : base(message)
        {
        }

        /// <inheritdoc/>
        public ApiKeyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        /// <inheritdoc/>
        protected ApiKeyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
