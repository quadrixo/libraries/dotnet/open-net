﻿using System.Security.Claims;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    public interface IApiKeyValidator
    {
        Task<ClaimsPrincipal> ValidateApiKey(string apiKey, ApiKeyOptions options);
    }
}
