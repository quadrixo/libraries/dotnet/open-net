﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Caching.Memory;
using Quadrixo.OpenNet.Authentication.ApiKey;

namespace ApiKeySample
{
    public class MyApiKeySessionStore : IApiKeyStore
    {
        private readonly IMemoryCache _cache;

        public MyApiKeySessionStore()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public Task StoreAsync(string apiKey, AuthenticationTicket ticket, CancellationToken cancellationToken)
        {
            var options = new MemoryCacheEntryOptions();
            var expiresUtc = ticket.Properties.ExpiresUtc;
            if (expiresUtc.HasValue)
            {
                options.SetAbsoluteExpiration(expiresUtc.Value);
            }
            options.SetSlidingExpiration(TimeSpan.FromSeconds(10)); // TODO: configurable.

            _cache.Set(apiKey, ticket, options);

            return Task.CompletedTask;
        }

        public Task<AuthenticationTicket?> RetrieveAsync(string apiKey, CancellationToken cancellationToken)
        {
            _cache.TryGetValue(apiKey, out AuthenticationTicket? ticket);
            return Task.FromResult(ticket);
        }

        public Task RemoveAsync(string apiKey, CancellationToken cancellationToken)
        {
            _cache.Remove(apiKey);
            return Task.CompletedTask;
        }
    }
}
