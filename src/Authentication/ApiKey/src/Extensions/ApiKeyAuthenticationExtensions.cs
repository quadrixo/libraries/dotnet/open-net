// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using Microsoft.AspNetCore.Http;

namespace Microsoft.AspNetCore.Authentication
{
    /// <summary>
    /// Extension methods for storing authentication apiKeys in <see cref="AuthenticationProperties"/>.
    /// </summary>
    public static class ApiKeyAuthenticationExtensions
    {
        private const string ApiKeyNamesKey = ".ApiKeyNames";
        private const string ApiKeyKeyPrefix = ".ApiKey.";

        /// <summary>
        /// Stores a set of authentication apiKeys, after removing any old apiKeys.
        /// </summary>
        /// <param name="properties">The <see cref="AuthenticationProperties"/> properties.</param>
        /// <param name="apiKeys">The apiKeys to store.</param>
        public static void StoreApiKeys(this AuthenticationProperties properties, IEnumerable<AuthenticationToken> apiKeys)
        {
            if (properties == null)
            {
                throw new ArgumentNullException(nameof(properties));
            }
            if (apiKeys == null)
            {
                throw new ArgumentNullException(nameof(apiKeys));
            }

            // Clear old apiKeys first
            var oldApiKeys = properties.GetApiKeys();
            foreach (var t in oldApiKeys)
            {
                properties.Items.Remove(ApiKeyKeyPrefix + t.Name);
            }
            properties.Items.Remove(ApiKeyNamesKey);

            var apiKeyNames = new List<string>();
            foreach (var apiKey in apiKeys)
            {
                if (apiKey.Name is null)
                {
                    throw new ArgumentNullException(nameof(apiKeys), "ApiKey name cannot be null.");
                }

                // REVIEW: should probably check that there are no ; in the apiKey name and throw or encode
                apiKeyNames.Add(apiKey.Name);
                properties.Items[ApiKeyKeyPrefix + apiKey.Name] = apiKey.Value;
            }
            if (apiKeyNames.Count > 0)
            {
                properties.Items[ApiKeyNamesKey] = string.Join(";", apiKeyNames.ToArray());
            }
        }

        /// <summary>
        /// Returns the value of a apiKey.
        /// </summary>
        /// <param name="properties">The <see cref="AuthenticationProperties"/> properties.</param>
        /// <param name="apiKeyName">The apiKey name.</param>
        /// <returns>The apiKey value.</returns>
        public static string? GetApiKeyValue(this AuthenticationProperties properties, string apiKeyName)
        {
            if (properties == null)
            {
                throw new ArgumentNullException(nameof(properties));
            }
            if (apiKeyName == null)
            {
                throw new ArgumentNullException(nameof(apiKeyName));
            }

            var apiKeyKey = ApiKeyKeyPrefix + apiKeyName;

            return properties.Items.TryGetValue(apiKeyKey, out var value) ? value : null;
        }

        /// <summary>
        /// Updates the value of a apiKey if already present.
        /// </summary>
        /// <param name="properties">The <see cref="AuthenticationProperties"/> to update.</param>
        /// <param name="apiKeyName">The apiKey name.</param>
        /// <param name="apiKeyValue">The apiKey value.</param>
        /// <returns><see langword="true"/> if the apiKey was updated, otherwise <see langword="false"/>.</returns>
        public static bool UpdateApiKeyValue(this AuthenticationProperties properties, string apiKeyName, string apiKeyValue)
        {
            if (properties == null)
            {
                throw new ArgumentNullException(nameof(properties));
            }
            if (apiKeyName == null)
            {
                throw new ArgumentNullException(nameof(apiKeyName));
            }

            var apiKeyKey = ApiKeyKeyPrefix + apiKeyName;
            if (!properties.Items.ContainsKey(apiKeyKey))
            {
                return false;
            }
            properties.Items[apiKeyKey] = apiKeyValue;
            return true;
        }

        /// <summary>
        /// Returns all of the <see cref="AuthenticationApiKey"/> instances contained in the properties.
        /// </summary>
        /// <param name="properties">The <see cref="AuthenticationProperties"/> properties.</param>
        /// <returns>The authentication apiKeys.</returns>
        public static IEnumerable<AuthenticationToken> GetApiKeys(this AuthenticationProperties properties)
        {
            if (properties == null)
            {
                throw new ArgumentNullException(nameof(properties));
            }

            var apiKeys = new List<AuthenticationToken>();
            if (properties.Items.TryGetValue(ApiKeyNamesKey, out var value) && !string.IsNullOrEmpty(value))
            {
                var apiKeyNames = value.Split(';');
                foreach (var name in apiKeyNames)
                {
                    var apiKey = properties.GetApiKeyValue(name);
                    if (apiKey != null)
                    {
                        apiKeys.Add(new AuthenticationToken { Name = name, Value = apiKey });
                    }
                }
            }

            return apiKeys;
        }

        /// <summary>
        /// Authenticates the request using the specified authentication scheme and returns the value for the apiKey.
        /// </summary>
        /// <param name="auth">The <see cref="IAuthenticationService"/>.</param>
        /// <param name="context">The <see cref="HttpContext"/> context.</param>
        /// <param name="apiKeyName">The name of the apiKey.</param>
        /// <returns>The value of the apiKey if present.</returns>
        public static Task<string?> GetApiKeyAsync(this IAuthenticationService auth, HttpContext context, string apiKeyName)
            => auth.GetApiKeyAsync(context, scheme: null, apiKeyName: apiKeyName);

        /// <summary>
        /// Authenticates the request using the specified authentication scheme and returns the value for the apiKey.
        /// </summary>
        /// <param name="auth">The <see cref="IAuthenticationService"/>.</param>
        /// <param name="context">The <see cref="HttpContext"/> context.</param>
        /// <param name="scheme">The name of the authentication scheme.</param>
        /// <param name="apiKeyName">The name of the apiKey.</param>
        /// <returns>The value of the apiKey if present.</returns>
        public static async Task<string?> GetApiKeyAsync(this IAuthenticationService auth, HttpContext context, string? scheme, string apiKeyName)
        {
            if (auth == null)
            {
                throw new ArgumentNullException(nameof(auth));
            }
            if (apiKeyName == null)
            {
                throw new ArgumentNullException(nameof(apiKeyName));
            }

            var result = await auth.AuthenticateAsync(context, scheme);
            return result?.Properties?.GetApiKeyValue(apiKeyName);
        }
    }
}
