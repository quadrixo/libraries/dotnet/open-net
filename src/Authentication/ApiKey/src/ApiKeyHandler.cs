﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Quadrixo.OpenNet.Authentication.ApiKey.Contexts;
using Quadrixo.OpenNet.Authentication.ApiKey.Exceptions;
using System.Text;
using System.Text.Encodings.Web;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    public class ApiKeyHandler : AuthenticationHandler<ApiKeyOptions>
    {
        public ApiKeyHandler(IOptionsMonitor<ApiKeyOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        /// <summary>
        /// The handler calls methods on the events which give the application control at certain points where processing is occurring.
        /// If it is not provided a default instance is supplied which does nothing when the methods are called.
        /// </summary>
        protected new ApiKeyEvents Events
        {
            get => (ApiKeyEvents)base.Events!;
            set => base.Events = value;
        }

        /// <inheritdoc />
        protected override Task<object> CreateEventsAsync() => Task.FromResult<object>(new ApiKeyEvents());

        protected async override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string? apiKey;

            try
            {
                // Give application opportunity to find from a different location, adjust, or reject API key
                var messageReceivedContext = new MessageReceivedContext(Context, Scheme, Options);

                // event can set the API key
                await Events.MessageReceived(messageReceivedContext);
                if (messageReceivedContext.Result != null)
                {
                    return messageReceivedContext.Result;
                }

                // If application retrieved API key from somewhere else, use that.
                apiKey = messageReceivedContext.ApiKey;

                if (string.IsNullOrEmpty(apiKey))
                {
                    string authorization = Request.Headers.Authorization;

                    // If no authorization header found, nothing to process further
                    if (string.IsNullOrEmpty(authorization))
                    {
                        return AuthenticateResult.NoResult();
                    }

                    if (authorization.StartsWith("ApiKey ", StringComparison.OrdinalIgnoreCase))
                    {
                        apiKey = authorization.Substring("ApiKey ".Length).Trim();
                    }

                    // If no API key found, no further work possible
                    if (string.IsNullOrEmpty(apiKey))
                    {
                        return AuthenticateResult.NoResult();
                    }
                }

                var ticket = await Options.SessionStore!.RetrieveAsync(apiKey);

                if (ticket == null)
                {
                    var principal = await Options.ApiKeyValidator.ValidateApiKey(apiKey, Options);
                    ticket = new AuthenticationTicket(principal, Scheme.Name);

                    await Options.SessionStore!.StoreAsync(apiKey, ticket);

                    Logger.ApiKeyValidationSucceeded();

                    var apiKeyValidatedContext = new ApiKeyValidatedContext(Context, Scheme, Options)
                    {
                        Principal = principal,
                        ApiKey = apiKey
                    };

                    await Events.ApiKeyValidated(apiKeyValidatedContext);
                    if (apiKeyValidatedContext.Result != null)
                    {
                        return apiKeyValidatedContext.Result;
                    }

                    if (Options.SaveApiKey)
                    {
                        apiKeyValidatedContext.Properties.StoreTokens(new[]
                        {
                            new AuthenticationToken { Name = "access_api_key", Value = apiKey }
                        });
                    }

                    apiKeyValidatedContext.Success();
                    return apiKeyValidatedContext.Result!;
                }

                return HandleRequestResult.Success(ticket);
            }
            catch (ApiKeyException ex)
            {
                Logger.ApiKeyValidationFailed(ex);

                var authenticationFailedContext = new AuthenticationFailedContext(Context, Scheme, Options)
                {
                    Exception = ex
                };

                await Events.AuthenticationFailed(authenticationFailedContext);
                if (authenticationFailedContext.Result != null)
                {
                    return authenticationFailedContext.Result;
                }

                return AuthenticateResult.Fail(authenticationFailedContext.Exception);

            }
            catch (Exception ex)
            {
                Logger.ErrorProcessingMessage(ex);

                var authenticationFailedContext = new AuthenticationFailedContext(Context, Scheme, Options)
                {
                    Exception = ex
                };

                await Events.AuthenticationFailed(authenticationFailedContext);
                if (authenticationFailedContext.Result != null)
                {
                    return authenticationFailedContext.Result;
                }

                throw;
            }
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            var authResult = await HandleAuthenticateOnceSafeAsync();
            var eventContext = new ApiKeyChallengeContext(Context, Scheme, Options, properties)
            {
                AuthenticateFailure = authResult?.Failure
            };

            // Avoid returning error=invalid_api_key if the error is not caused by an authentication failure (e.g missing API key).
            if (Options.IncludeErrorDetails && eventContext.AuthenticateFailure != null)
            {
                eventContext.Error = "invalid_api_key";
                eventContext.ErrorDescription = CreateErrorDescription(eventContext.AuthenticateFailure);
            }

            await Events.Challenge(eventContext);
            if (eventContext.Handled)
            {
                return;
            }

            Response.StatusCode = 401;

            if (string.IsNullOrEmpty(eventContext.Error) &&
                string.IsNullOrEmpty(eventContext.ErrorDescription) &&
                string.IsNullOrEmpty(eventContext.ErrorUri))
            {
                Response.Headers.Append(HeaderNames.WWWAuthenticate, Options.Challenge);
            }
            else
            {
                // https://tools.ietf.org/html/rfc6750#section-3.1
                // WWW-Authenticate: Bearer realm="example", error="invalid_api_key", error_description="The API key has been revoked"
                var builder = new StringBuilder(Options.Challenge);
                if (Options.Challenge.IndexOf(' ') > 0)
                {
                    // Only add a comma after the first param, if any
                    builder.Append(',');
                }
                if (!string.IsNullOrEmpty(eventContext.Error))
                {
                    builder.Append(" error=\"");
                    builder.Append(eventContext.Error);
                    builder.Append('\"');
                }
                if (!string.IsNullOrEmpty(eventContext.ErrorDescription))
                {
                    if (!string.IsNullOrEmpty(eventContext.Error))
                    {
                        builder.Append(',');
                    }

                    builder.Append(" error_description=\"");
                    builder.Append(eventContext.ErrorDescription);
                    builder.Append('\"');
                }
                if (!string.IsNullOrEmpty(eventContext.ErrorUri))
                {
                    if (!string.IsNullOrEmpty(eventContext.Error) ||
                        !string.IsNullOrEmpty(eventContext.ErrorDescription))
                    {
                        builder.Append(',');
                    }

                    builder.Append(" error_uri=\"");
                    builder.Append(eventContext.ErrorUri);
                    builder.Append('\"');
                }

                Response.Headers.Append(HeaderNames.WWWAuthenticate, builder.ToString());
            }
        }

        protected override Task HandleForbiddenAsync(AuthenticationProperties properties)
        {
            var forbiddenContext = new ForbiddenContext(Context, Scheme, Options);
            Response.StatusCode = 403;
            return Events.Forbidden(forbiddenContext);
        }

        private static string CreateErrorDescription(Exception authFailure)
        {
            IReadOnlyCollection<Exception> exceptions;
            if (authFailure is AggregateException agEx)
            {
                exceptions = agEx.InnerExceptions;
            }
            else
            {
                exceptions = new[] { authFailure };
            }

            var messages = new List<string>(exceptions.Count);

            foreach (var ex in exceptions)
            {
                // Order sensitive, some of these exceptions derive from others
                // and we want to display the most specific message possible.
                switch (ex)
                {
                    case ExceededQuotaApiKeyException:
                        messages.Add("Quota of the API key exceeded");
                        break;
                    case NotFoundApiKeyException:
                        messages.Add("API key not be found");
                        break;
                    case RevokedApiKeyException:
                        messages.Add("API key revoked");
                        break;
                    case ApiKeyException:
                        messages.Add(ex.Message);
                        break;
                }
            }

            return string.Join("; ", messages);
        }
    }
}
