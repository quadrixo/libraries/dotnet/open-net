﻿using Quadrixo.OpenNet.Authentication.ApiKey.Contexts;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    /// <summary>
    /// Specifies events which the <see cref="ApiKeyHandler"/> invokes to enable developer control over the authentication process.
    /// </summary>
    public class ApiKeyEvents
    {
        /// <summary>
        /// Invoked if authentication fails during request processing. The exceptions will be re-thrown after this event unless suppressed.
        /// </summary>
        public Func<AuthenticationFailedContext, Task> OnAuthenticationFailed { get; set; } = context => Task.CompletedTask;

        /// <summary>
        /// Invoked if Authorization fails and results in a Forbidden response.
        /// </summary>
        public Func<ForbiddenContext, Task> OnForbidden { get; set; } = context => Task.CompletedTask;

        /// <summary>
        /// Invoked when a protocol message is first received.
        /// </summary>
        public Func<MessageReceivedContext, Task> OnMessageReceived { get; set; } = context => Task.CompletedTask;

        /// <summary>
        /// Invoked after the API key has passed validation and a ClaimsIdentity has been generated.
        /// </summary>
        public Func<ApiKeyValidatedContext, Task> OnApiKeyValidated { get; set; } = context => Task.CompletedTask;

        /// <summary>
        /// Invoked before a challenge is sent back to the caller.
        /// </summary>
        public Func<ApiKeyChallengeContext, Task> OnChallenge { get; set; } = context => Task.CompletedTask;

        /// <summary>
        /// Invoked if exceptions are thrown during request processing. The exceptions will be re-thrown after this event unless suppressed.
        /// </summary>
        public virtual Task AuthenticationFailed(AuthenticationFailedContext context) => OnAuthenticationFailed(context);

        /// <summary>
        /// Invoked if Authorization fails and results in a Forbidden response
        /// </summary>
        public virtual Task Forbidden(ForbiddenContext context) => OnForbidden(context);

        /// <summary>
        /// Invoked when a protocol message is first received.
        /// </summary>
        public virtual Task MessageReceived(MessageReceivedContext context) => OnMessageReceived(context);

        /// <summary>
        /// Invoked after the security API key has passed validation and a ClaimsIdentity has been generated.
        /// </summary>
        public virtual Task ApiKeyValidated(ApiKeyValidatedContext context) => OnApiKeyValidated(context);

        /// <summary>
        /// Invoked before a challenge is sent back to the caller.
        /// </summary>
        public virtual Task Challenge(ApiKeyChallengeContext context) => OnChallenge(context);
    }
}
