﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Contexts
{
    /// <summary>
    /// A context for <see cref="ApiKeyEvents.OnForbidden"/>.
    /// </summary>
    public class MessageReceivedContext : ResultContext<ApiKeyOptions>
    {
        /// <summary>
        /// Initializes a new instance of <see cref="ForbiddenContext"/>.
        /// </summary>
        /// <inheritdoc />
        public MessageReceivedContext(HttpContext context, AuthenticationScheme scheme, ApiKeyOptions options)
            : base(context, scheme, options)
        {
        }

        /// <summary>
        /// API key. This will give the application an opportunity to retrieve an API key from an alternative location.
        /// </summary>
        public string? ApiKey { get; set; }
    }
}
