﻿using System.Runtime.Serialization;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Exceptions
{
    internal class ExceededQuotaApiKeyException : ApiKeyException
    {
        public ExceededQuotaApiKeyException()
        {
        }

        public ExceededQuotaApiKeyException(string? message) : base(message)
        {
        }

        public ExceededQuotaApiKeyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected ExceededQuotaApiKeyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
