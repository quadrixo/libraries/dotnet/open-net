// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

namespace Microsoft.Extensions.Logging
{
    internal static class LoggingExtensions
    {
        private static readonly Action<ILogger, Exception> _apiKeyValidationFailed = LoggerMessage.Define(
            eventId: new EventId(1, "ApiKeyValidationFailed"),
            logLevel: LogLevel.Information,
            formatString: "Failed to validate the API key.");
        private static readonly Action<ILogger, Exception?> _apiKeyValidationSucceeded = LoggerMessage.Define(
                eventId: new EventId(2, "ApiKeyValidationSucceeded"),
                logLevel: LogLevel.Debug,
                formatString: "Successfully validated the token.");
        private static readonly Action<ILogger, Exception> _errorProcessingMessage = LoggerMessage.Define(
                eventId: new EventId(3, "ProcessingMessageFailed"),
                logLevel: LogLevel.Error,
                formatString: "Exception occurred while processing message.");

        public static void ApiKeyValidationFailed(this ILogger logger, Exception ex)
            => _apiKeyValidationFailed(logger, ex);

        public static void ApiKeyValidationSucceeded(this ILogger logger)
            => _apiKeyValidationSucceeded(logger, null);

        public static void ErrorProcessingMessage(this ILogger logger, Exception ex)
            => _errorProcessingMessage(logger, ex);
    }
}
