﻿using System.Runtime.Serialization;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Exceptions
{
    /// <summary>
    /// This exception is thrown when the API key is not found.
    /// </summary>
    public class NotFoundApiKeyException : ApiKeyException
    {
        public NotFoundApiKeyException()
        {
        }

        public NotFoundApiKeyException(string? message) : base(message)
        {
        }

        public NotFoundApiKeyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected NotFoundApiKeyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
