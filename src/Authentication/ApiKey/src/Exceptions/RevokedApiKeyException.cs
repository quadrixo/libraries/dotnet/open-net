﻿using System.Runtime.Serialization;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Exceptions
{
    /// <summary>
    /// This exception is thrown when the API key has been revoked.
    /// </summary>
    public class RevokedApiKeyException : ApiKeyException
    {
        public RevokedApiKeyException()
        {
        }

        public RevokedApiKeyException(string? message) : base(message)
        {
        }

        public RevokedApiKeyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected RevokedApiKeyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
