﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Contexts
{
    /// <summary>
    /// A <see cref="ResultContext{TOptions}"/> when authentication has failed.
    /// </summary>
    public class AuthenticationFailedContext : ResultContext<ApiKeyOptions>
    {
        public AuthenticationFailedContext(HttpContext context, AuthenticationScheme scheme, ApiKeyOptions options)
            : base(context, scheme, options)
        {
        }

        /// <summary>
        /// Gets or sets the exception associated with the authentication failure.
        /// </summary>
        public Exception Exception { get; set; } = default!;
    }
}
