﻿using Microsoft.AspNetCore.Authentication;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    public interface IApiKeyStore
    {
        /// <summary>
        /// Tells the store that the given identity should be updated.
        /// </summary>
        /// <param name="apiKey">The key associated with the identity.</param>
        /// <param name="ticket">The identity information to store.</param>
        /// <returns></returns>
        Task StoreAsync(string apiKey, AuthenticationTicket ticket) => StoreAsync(apiKey, ticket, CancellationToken.None);

        /// <summary>
        /// Tells the store that the given identity should be updated.
        /// </summary>
        /// <param name="apiKey">The key associated with the identity.</param>
        /// <param name="ticket">The identity information to store.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task StoreAsync(string apiKey, AuthenticationTicket ticket, CancellationToken cancellationToken);

        /// <summary>
        /// Retrieves an identity from the store for the given key.
        /// </summary>
        /// <param name="apiKey">The key associated with the identity.</param>
        /// <returns>The identity associated with the given key, or <c>null</c> if not found.</returns>
        Task<AuthenticationTicket?> RetrieveAsync(string apiKey) => RetrieveAsync(apiKey, CancellationToken.None);

        /// <summary>
        /// Retrieves an identity from the store for the given key.
        /// </summary>
        /// <param name="apiKey">The key associated with the identity.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The identity associated with the given key, or <c>null</c> if not found.</returns>
        Task<AuthenticationTicket?> RetrieveAsync(string apiKey, CancellationToken cancellationToken);

        /// <summary>
        /// Remove the identity associated with the given key.
        /// </summary>
        /// <param name="apiKey">The key associated with the identity.</param>
        /// <returns></returns>
        Task RemoveAsync(string apiKey) => RemoveAsync(apiKey, CancellationToken.None);

        /// <summary>
        /// Remove the identity associated with the given key.
        /// </summary>
        /// <param name="apiKey">The key associated with the identity.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task RemoveAsync(string apiKey, CancellationToken cancellationToken);
    }
}
