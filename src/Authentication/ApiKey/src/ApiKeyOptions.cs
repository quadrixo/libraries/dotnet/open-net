﻿using Microsoft.AspNetCore.Authentication;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    /// <summary>
    /// Configuration options for <see cref="ApiKeyOptions"/>.
    /// </summary>
    public class ApiKeyOptions : AuthenticationSchemeOptions
    {
        private readonly ApiKeyValidator _defaultValidator = new ApiKeyValidator();

        public ApiKeyOptions()
        {
            ApiKeyValidator = _defaultValidator;
        }

        /// <summary>
        /// Gets or sets the challenge to put in the "WWW-Authenticate" header.
        /// </summary>
        public string Challenge { get; set; } = ApiKeyDefaults.AuthenticationScheme;

        /// <summary>
        /// The object provided by the application to process events raised by the API key authentication handler.
        /// The application may implement the interface fully, or it may create an instance of ApiKeyEvents
        /// and assign delegates only to the events it wants to process.
        /// </summary>
        public new ApiKeyEvents Events
        {
            get { return (ApiKeyEvents)base.Events!; }
            set { base.Events = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IApiKeyValidator"/> used to validate API key.
        /// </summary>
        public IApiKeyValidator ApiKeyValidator { get; set; }

        /// <summary>
        /// Defines whether the API key should be stored in the
        /// <see cref="AuthenticationProperties"/> after a successful authorization.
        /// </summary>
        public bool SaveApiKey { get; set; } = true;

        /// <summary>
        /// Defines whether the token validation errors should be returned to the caller.
        /// Enabled by default, this option can be disabled to prevent the API key handler
        /// from returning an error and an error_description in the WWW-Authenticate header.
        /// </summary>
        public bool IncludeErrorDetails { get; set; } = true;

        /// <summary>
        /// An optional container in which to store the identity across requests. When used, only a session identifier is sent
        /// to the client. This can be used to mitigate potential problems with very large identities.
        /// </summary>
        public IApiKeyStore? SessionStore { get; set; }

        /// <summary>
        /// Expire time after which the session should be reloaded (and API key revalidated).
        /// </summary>
        public TimeSpan ExpireTimeSpan { get; set; } = ApiKeyDefaults.ExpireTimeSpan;
    }
}
