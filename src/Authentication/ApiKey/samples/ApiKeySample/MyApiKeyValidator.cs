﻿using Quadrixo.OpenNet.Authentication.ApiKey;
using Quadrixo.OpenNet.Authentication.ApiKey.Exceptions;
using System.Security.Claims;

namespace ApiKeySample
{
    internal class MyApiKeyValidator : IApiKeyValidator
    {
        public async Task<ClaimsPrincipal> ValidateApiKey(string apiKey, ApiKeyOptions options)
        {
            await Task.Delay(500);

            if (apiKey == "revoked")
            {
                throw new RevokedApiKeyException();
            }

            var identity = new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, apiKey),
                new Claim(ClaimTypes.Name, "John Doe"),
                new Claim(ClaimTypes.Role, "User")
            },
            ApiKeyDefaults.AuthenticationScheme);

            return new ClaimsPrincipal(identity);
        }
    }
}
