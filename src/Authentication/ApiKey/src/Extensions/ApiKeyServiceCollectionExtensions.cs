﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Quadrixo.OpenNet.Authentication.ApiKey;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Extension methods to configure API key authentication.
    /// </summary>
    public static class ApiKeyServiceCollectionExtensions
    {/// <summary>
     /// Adds cookie authentication to <see cref="AuthenticationBuilder"/> using the default scheme.
     /// The default scheme is specified by <see cref="ApiKeyDefaults.AuthenticationScheme"/>.
     /// </summary>
     /// <param name="builder">The <see cref="AuthenticationBuilder"/>.</param>
     /// <returns>A reference to <paramref name="builder"/> after the operation has completed.</returns>
        public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder)
            => builder.AddApiKey(ApiKeyDefaults.AuthenticationScheme, _ => { });

        /// <summary>
        /// Adds cookie authentication to <see cref="AuthenticationBuilder"/> using the specified scheme.
        /// </summary>
        /// <param name="builder">The <see cref="AuthenticationBuilder"/>.</param>
        /// <param name="authenticationScheme">The authentication scheme.</param>
        /// <returns>A reference to <paramref name="builder"/> after the operation has completed.</returns>
        public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, string authenticationScheme)
            => builder.AddApiKey(authenticationScheme, configureOptions: null!);

        /// <summary>
        /// Adds cookie authentication to <see cref="AuthenticationBuilder"/> using the default scheme.
        /// The default scheme is specified by <see cref="ApiKeyDefaults.AuthenticationScheme"/>.
        /// </summary>
        /// <param name="builder">The <see cref="AuthenticationBuilder"/>.</param>
        /// <param name="configureOptions">A delegate to configure <see cref="ApiKeyOptions"/>.</param>
        /// <returns>A reference to <paramref name="builder"/> after the operation has completed.</returns>
        public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, Action<ApiKeyOptions> configureOptions)
            => builder.AddApiKey(ApiKeyDefaults.AuthenticationScheme, configureOptions);

        /// <summary>
        /// Adds cookie authentication to <see cref="AuthenticationBuilder"/> using the specified scheme.
        /// </summary>
        /// <param name="builder">The <see cref="AuthenticationBuilder"/>.</param>
        /// <param name="authenticationScheme">The authentication scheme.</param>
        /// <param name="configureOptions">A delegate to configure <see cref="ApiKeyOptions"/>.</param>
        /// <returns>A reference to <paramref name="builder"/> after the operation has completed.</returns>
        public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, string authenticationScheme, Action<ApiKeyOptions> configureOptions)
            => builder.AddApiKey(authenticationScheme, displayName: null, configureOptions: configureOptions);

        /// <summary>
        /// Adds cookie authentication to <see cref="AuthenticationBuilder"/> using the specified scheme.
        /// </summary>
        /// <param name="builder">The <see cref="AuthenticationBuilder"/>.</param>
        /// <param name="authenticationScheme">The authentication scheme.</param>
        /// <param name="displayName">A display name for the authentication handler.</param>
        /// <param name="configureOptions">A delegate to configure <see cref="ApiKeyOptions"/>.</param>
        /// <returns>A reference to <paramref name="builder"/> after the operation has completed.</returns>
        public static AuthenticationBuilder AddApiKey(this AuthenticationBuilder builder, string authenticationScheme, string? displayName, Action<ApiKeyOptions> configureOptions)
        {
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<ApiKeyOptions>, PostConfigureApiKeyAuthenticationOptions>());
            return builder.AddScheme<ApiKeyOptions, ApiKeyHandler>(authenticationScheme, displayName, configureOptions);
        }
    }
}