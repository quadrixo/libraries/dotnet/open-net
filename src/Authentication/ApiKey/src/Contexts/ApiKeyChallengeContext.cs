﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Contexts
{
    /// <summary>
    /// A <see cref="PropertiesContext{TOptions}"/> when access to a resource authenticated using API key is challenged.
    /// </summary>
    public class ApiKeyChallengeContext : PropertiesContext<ApiKeyOptions>
    {
        public ApiKeyChallengeContext(HttpContext context, AuthenticationScheme scheme, ApiKeyOptions options, AuthenticationProperties? properties)
            : base(context, scheme, options, properties)
        {
        }

        /// <summary>
        /// Any failures encountered during the authentication process.
        /// </summary>
        public Exception? AuthenticateFailure { get; set; }

        /// <summary>
        /// Gets or sets the "error" value returned to the caller as part
        /// of the WWW-Authenticate header. This property may be null when
        /// <see cref="ApiKeyOptions.IncludeErrorDetails"/> is set to <c>false</c>.
        /// </summary>
        public string? Error { get; set; }

        /// <summary>
        /// Gets or sets the "error_description" value returned to the caller as part
        /// of the WWW-Authenticate header. This property may be null when
        /// <see cref="ApiKeyOptions.IncludeErrorDetails"/> is set to <c>false</c>.
        /// </summary>
        public string? ErrorDescription { get; set; }

        /// <summary>
        /// Gets or sets the "error_uri" value returned to the caller as part of the
        /// WWW-Authenticate header. This property is always null unless explicitly set.
        /// </summary>
        public string? ErrorUri { get; set; }

        /// <summary>
        /// If true, will skip any default logic for this challenge.
        /// </summary>
        public bool Handled { get; private set; }

        /// <summary>
        /// Skips any default logic for this challenge.
        /// </summary>
        public void HandleResponse() => Handled = true;
    }
}
