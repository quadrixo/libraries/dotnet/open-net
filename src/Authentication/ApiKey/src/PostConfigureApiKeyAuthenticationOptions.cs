﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;

namespace Quadrixo.OpenNet.Authentication.ApiKey
{
    public class PostConfigureApiKeyAuthenticationOptions : IPostConfigureOptions<ApiKeyOptions>
    {
        public void PostConfigure(string name, ApiKeyOptions options)
        {
            options.SessionStore ??= new NullApiKeyStore();
        }

        private class NullApiKeyStore : IApiKeyStore
        {
            public Task RemoveAsync(string apiKey, CancellationToken cancellationToken) => Task.CompletedTask;

            public Task<AuthenticationTicket?> RetrieveAsync(string apiKey, CancellationToken cancellationToken) => Task.FromResult<AuthenticationTicket?>(null);

            public Task StoreAsync(string apiKey, AuthenticationTicket ticket, CancellationToken cancellationToken) => Task.CompletedTask;
        }
    }
}
