﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Contexts
{
    public class ApiKeyValidatedContext : ResultContext<ApiKeyOptions>
    {
        public ApiKeyValidatedContext(HttpContext context, AuthenticationScheme scheme, ApiKeyOptions options)
            : base(context, scheme, options)
        {
        }

        /// <summary>
        /// API key. This will give the application an opportunity to retrieve an API key from an alternative location.
        /// </summary>
        public string ApiKey { get; internal set; } = default!;
    }
}
