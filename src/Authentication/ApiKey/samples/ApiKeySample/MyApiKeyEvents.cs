﻿using Quadrixo.OpenNet.Authentication.ApiKey;

namespace ApiKeySample
{
    public class MyApiKeyEvents : ApiKeyEvents
    {
        private readonly ILogger _logger;

        public MyApiKeyEvents(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger(nameof(ApiKeySample));

            OnMessageReceived = ctx => Task.Run(() => _logger.LogInformation("Message received"));
            OnChallenge = ctx => Task.Run(() => _logger.LogInformation("Request challenged"));
            OnForbidden = ctx => Task.Run(() => _logger.LogInformation("Request forbidden"));
            OnAuthenticationFailed = ctx => Task.Run(() => _logger.LogInformation("Authentication failed"));
            OnApiKeyValidated = ctx => Task.Run(() => _logger.LogInformation($"API key '{ctx.ApiKey}' validated"));
        }
    }
}
