﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace Quadrixo.OpenNet.Authentication.ApiKey.Contexts
{
    /// <summary>
    /// A context for <see cref="ApiKeyEvents.OnForbidden"/>.
    /// </summary>
    public class ForbiddenContext : ResultContext<ApiKeyOptions>
    {
        /// <summary>
        /// Initializes a new instance of <see cref="ForbiddenContext"/>.
        /// </summary>
        /// <inheritdoc />
        public ForbiddenContext(HttpContext context, AuthenticationScheme scheme, ApiKeyOptions options)
            : base(context, scheme, options)
        {
        }
    }
}
