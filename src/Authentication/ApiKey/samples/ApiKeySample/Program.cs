using ApiKeySample;
using Quadrixo.OpenNet.Authentication.ApiKey;

var builder = WebApplication.CreateBuilder(args);
builder.Logging.AddConsole();

// Add services to the container.
builder.Services
    .AddSingleton<IApiKeyStore, MyApiKeySessionStore>()
    .AddTransient<MyApiKeyEvents>()
    .AddAuthentication(ApiKeyDefaults.AuthenticationScheme)
    .AddApiKey(options =>
    {
        options.ApiKeyValidator = new MyApiKeyValidator();
        options.EventsType = typeof(MyApiKeyEvents);
    });

builder.Services
    .AddOptions<ApiKeyOptions>(ApiKeyDefaults.AuthenticationScheme)
    .Configure<IApiKeyStore>((options, sessionStore) => options.SessionStore = sessionStore);

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
